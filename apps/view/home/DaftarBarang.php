
<a href="<?= BASE_URL.'index.php?r=home/insertBarang' ?>" type="button" class="btn btn-primary">Tambah Barang</a>    

<br> </br>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nama</th>
            <th>Jumlah</th>
            <th>Update/Delete</th>
        </tr>
        </thead>
        <?php foreach($data as $item) : ?>
        <tr scope="row">
            <td><?= $item['id'] ?></td>
            <td><?= $item['nama'] ?></td>
            <td><span class="badge rounded-pill text-bg-<?= $item['qty']>50? 'success' : 'danger' ?>"><?= $item['qty'] ?></span></td>
            <td>
                <a href="<?= BASE_URL.'index.php?r=home/updateBarang/'.$item['id']?>" class="badge rounded-pill text-bg-info">Update</a>
                <a href="<?= BASE_URL.'index.php?r=home/deleteBarang/'.$item['id']?>" class="badge rounded-pill text-bg-warning" onclick="return confirm('Apakah Anda yakin untuk menghapus data barang?')">Delete</a>
            </td>
        </tr>
        <?php endforeach ?>
    </table>    
